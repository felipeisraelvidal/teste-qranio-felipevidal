//
//  MarvelComic.swift
//  Teste Qranio
//
//  Created by Felipe Israel on 19/01/20.
//  Copyright © 2020 Click. All rights reserved.
//

import UIKit

class MarvelComic: NSObject {
    
    var id: String!
    
    var thumbnail: String?
    var title: String!
    var desc: String?
    
    init(dict: [String : Any]) {
        
        self.id = dict["id"] as? String
        
        if let thumbnail = dict["thumbnail"] as? [String : Any], let path = thumbnail["path"] as? String, let fileExtension = thumbnail["extension"] as? String {
            self.thumbnail = "\(path)/landscape_incredible.\(fileExtension)"
        }
        
        self.title = dict["title"] as? String
        
        super.init()
    }
    
}
