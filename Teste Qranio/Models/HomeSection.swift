//
//  HomeSection.swift
//  Teste Qranio
//
//  Created by Felipe Israel on 19/01/20.
//  Copyright © 2020 Click. All rights reserved.
//

import UIKit

// MARK: - Home Section
struct HomeSection {
    var title: String? = nil
    var items: [HomeSection.Item]
    
    // MARK: - Home Item
    struct Item {
        var gradient: [UIColor]
        var icon: UIImage
        var title: String
        var action: () -> Void
    }
}
