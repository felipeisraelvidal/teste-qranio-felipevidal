//
//  ApiKeys.swift
//  Teste Qranio
//
//  Created by Felipe Israel on 19/01/20.
//  Copyright © 2020 Click. All rights reserved.
//

import UIKit

struct ApiKeys {
    
    struct Marvel {
        static let public_key = "3fd86871e60a34060e3835b703c66e55"
        static let private_key = "e285209c530274eb24414aefa61bd67d192e578c"
    }
    
}
