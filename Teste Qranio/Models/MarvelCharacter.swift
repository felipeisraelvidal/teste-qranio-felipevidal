//
//  MarvelCharacter.swift
//  Teste Qranio
//
//  Created by Felipe Israel on 19/01/20.
//  Copyright © 2020 Click. All rights reserved.
//

import UIKit

class MarvelCharacter: NSObject {
    
    var id: String!
    
    var thumbnail: String?
    var name: String!
    var desc: String?
    
    init(dict: [String : Any]) {
        self.id = dict["id"] as? String
        
        if let thumbnail = dict["thumbnail"] as? [String : Any], let path = thumbnail["path"] as? String, let fileExtension = thumbnail["extension"] as? String {
            self.thumbnail = "\(path)/standard_fantastic.\(fileExtension)"
        }
        
        self.name = dict["name"] as? String
        if let desc = dict["description"] as? String, !desc.isEmpty {
            self.desc = desc
        }
        
        super.init()
    }
    
}
