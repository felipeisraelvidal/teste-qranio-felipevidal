//
//  LoginViewController.swift
//  Teste Qranio
//
//  Created by Felipe Israel on 20/01/20.
//  Copyright © 2020 Click. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    
    @IBOutlet private weak var credentialsView: UIView!
    @IBOutlet private weak var separator: UIView!
    
    @IBOutlet private weak var txtEmail: UITextField!
    @IBOutlet private weak var txtPassword: UITextField!
    
    @IBOutlet private weak var containerBottom: NSLayoutConstraint!

    override func viewDidLoad() {
        super.viewDidLoad()

        credentialsView.layer.cornerRadius = 16
        credentialsView.clipsToBounds = true
        credentialsView.layer.borderWidth = 0.5
        if #available(iOS 13.0, *) {
            credentialsView.layer.borderColor = UIColor.tertiaryLabel.cgColor
            separator.backgroundColor = UIColor.tertiaryLabel
        } else {
            credentialsView.layer.borderColor = UIColor.lightGray.cgColor
            separator.backgroundColor = .lightGray
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event)
        view.endEditing(true)
    }
    
    @objc func keyboardWillShow(_ notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue, let duration = notification.userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as? TimeInterval {
            var keyboardHeight = keyboardSize.height
            
            if #available(iOS 11.0, *) {
                keyboardHeight -= view.safeAreaInsets.bottom
            }
            
            containerBottom.constant = keyboardHeight
            
            UIView.animate(withDuration: duration) {
                self.view.layoutIfNeeded()
            }
        }
    }
    
    @objc func keyboardWillHide(_ notification: NSNotification) {
        if let duration = notification.userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as? TimeInterval {
            containerBottom.constant = 0
            
            UIView.animate(withDuration: duration) {
                self.view.layoutIfNeeded()
            }
        }
    }

}

extension LoginViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case txtEmail:
            txtPassword.becomeFirstResponder()
        case txtPassword:
            txtPassword.resignFirstResponder()
        default:
            break
        }
        return true
    }
    
}
