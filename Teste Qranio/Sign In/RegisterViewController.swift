//
//  RegisterViewController.swift
//  Teste Qranio
//
//  Created by Felipe Israel on 20/01/20.
//  Copyright © 2020 Click. All rights reserved.
//

import UIKit
import Firebase

class RegisterViewController: UIViewController {
    
    @IBOutlet private var credentialsViews: [UIView]!
    @IBOutlet private var separators: [UIView]!
    
    @IBOutlet private weak var txtEmail: UITextField!
    @IBOutlet private weak var txtPassword: UITextField!
    @IBOutlet private weak var txtConfirmPassword: UITextField!
    
    @IBOutlet private weak var containerBottom: NSLayoutConstraint!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        credentialsViews.forEach { (view) in
            view.layer.cornerRadius = 16
            view.clipsToBounds = true
            view.layer.borderWidth = 0.5
            if #available(iOS 13.0, *) {
                view.layer.borderColor = UIColor.tertiaryLabel.cgColor
            } else {
                view.layer.borderColor = UIColor.lightGray.cgColor
            }
        }
        
        separators.forEach { (view) in
            if #available(iOS 13.0, *) {
                view.backgroundColor = UIColor.tertiaryLabel
            } else {
                view.backgroundColor = .lightGray
            }
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event)
        view.endEditing(true)
    }
    
    @objc func keyboardWillShow(_ notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue, let duration = notification.userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as? TimeInterval {
            var keyboardHeight = keyboardSize.height
            
            if #available(iOS 11.0, *) {
                keyboardHeight -= view.safeAreaInsets.bottom
            }
            
            containerBottom.constant = keyboardHeight
            
            UIView.animate(withDuration: duration) {
                self.view.layoutIfNeeded()
            }
        }
    }
    
    @objc func keyboardWillHide(_ notification: NSNotification) {
        if let duration = notification.userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as? TimeInterval {
            containerBottom.constant = 0
            
            UIView.animate(withDuration: duration) {
                self.view.layoutIfNeeded()
            }
        }
    }
    
    @IBAction func createAccount(_ sender: UIButton) {
        register()
    }
    
    private func register() {
        if let email = txtEmail.text, !email.isEmpty,
            let password = txtPassword.text, !password.isEmpty,
            let confirmPassword = txtConfirmPassword.text, !confirmPassword.isEmpty {
            if password == confirmPassword {
                view.endEditing(true)
                
                let loading = presentLoading(title: "Criando Conta...")
                
                Auth.auth().createUser(withEmail: email, password: password) { (result, error) in
                    guard let result = result else { return }
                    
                    let data: [String : Any] = [
                        "email": email
                    ]
                    
                    Firestore.firestore().collection("accounts").document(result.user.uid).setData(data) { (err) in
                        if let err = err {
                            loading.dismiss(animated: true) {
                                self.showAlert(title: "Oops...", message: err.localizedDescription)
                            }
                        } else {
                            loading.dismiss(animated: true) {
                                self.dismiss(animated: true, completion: nil)
                            }
                        }
                    }
                }
            } else {
                self.showAlert(title: "Oops...", message: "Suas senhas não coincidem.")
            }
        }
    }

}

extension RegisterViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case txtEmail:
            txtPassword.becomeFirstResponder()
        case txtPassword:
            txtConfirmPassword.becomeFirstResponder()
        case txtConfirmPassword:
            register()
        default:
            break
        }
        return true
    }
    
}
