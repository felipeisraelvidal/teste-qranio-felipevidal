//
//  Extensions.swift
//  Teste Qranio
//
//  Created by Felipe Israel on 19/01/20.
//  Copyright © 2020 Click. All rights reserved.
//

import UIKit

let imageCache = NSCache<AnyObject, AnyObject>()

extension UIViewController {
    
    func showAlert(title: String, message: String?, actionTitle: String? = nil, action: (() -> Void)? = nil) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        if action != nil {
            alertController.addAction(UIAlertAction(title: actionTitle ?? "Fechar", style: .default, handler: { (_) in
                action?()
            }))
        } else {
            alertController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        }
        
        present(alertController, animated: true, completion: nil)
    }
    
    func presentLoading(title: String) -> UIAlertController {
        let alertController = UIAlertController(title: title, message: nil, preferredStyle: .alert)
        present(alertController, animated: true, completion: nil)
        return alertController
    }
    
}

public extension UIColor {
    
  convenience init(r: CGFloat, g: CGFloat, b: CGFloat) {
        self.init(red: r/255, green: g/255, blue: b/255, alpha: 1)
    }
    
    static var customBackgroundColor: UIColor = {
        return UIColor(r: 239, g: 239, b: 244)
    }()
    
}

extension UIColor {
    convenience init(hexString: String, alpha: CGFloat = 1) {
        assert(hexString[hexString.startIndex] == "#", "Expected hex string of format #RRGGBB")
        
        let scanner = Scanner(string: hexString)
        scanner.scanLocation = 1  // skip #
        
        var rgb: UInt32 = 0
        scanner.scanHexInt32(&rgb)
        
        self.init(
            red:   CGFloat((rgb & 0xFF0000) >> 16)/255.0,
            green: CGFloat((rgb &   0xFF00) >>  8)/255.0,
            blue:  CGFloat((rgb &     0xFF)      )/255.0,
            alpha: alpha)
    }
}

extension UIColor {
    func toHexString() -> String {
        var r:CGFloat = 0
        var g:CGFloat = 0
        var b:CGFloat = 0
        var a:CGFloat = 0
        
        getRed(&r, green: &g, blue: &b, alpha: &a)
        
        let rgb:Int = (Int)(r*255)<<16 | (Int)(g*255)<<8 | (Int)(b*255)<<0
        
        return String(format:"#%06x", rgb)
    }
}

extension UIView {
    func setGradientBackgroundColor(colorOne: UIColor, colorTow: UIColor) {
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = bounds
        gradientLayer.colors = [colorOne.cgColor, colorTow.cgColor]
        gradientLayer.locations = [0.0, 1.0]
        gradientLayer.endPoint = CGPoint(x: 1.0, y: 1.0)
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.0)
        
        layer.insertSublayer(gradientLayer, at: 0)
    }
}

extension UIImageView {
    
    func loadImageUsingCacheWith(urlString: String, completionHandler: ((Bool) -> Void)? = nil) {
        // Check cache for image first
        if let cachedImage = imageCache.object(forKey: urlString as AnyObject) as? UIImage {
            self.image = cachedImage
            completionHandler?(true)
            return
        }
        
        // Otherwise fire off a new download
        let url = URL(string: urlString)
        URLSession.shared.dataTask(with: url!) { (data, response, error) in
            // Download hit an error so lets return out
            if error != nil {
                print(error!)
                completionHandler?(false)
                return
            }
            
            DispatchQueue.main.async {
                if let downloadedImage = UIImage(data: data!) {
                    imageCache.setObject(downloadedImage, forKey: urlString as AnyObject)
                    completionHandler?(true)
                    self.image = downloadedImage
                }
            }
        }.resume()
    }
    
}
