//
//  ComicsMarvelViewController.swift
//  Teste Qranio
//
//  Created by Felipe Israel on 19/01/20.
//  Copyright © 2020 Click. All rights reserved.
//

import UIKit
import Alamofire

class ComicsMarvelViewController: UITableViewController {
    
    private var comics: [MarvelComic] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Quadrinhos"
        
        if #available(iOS 11.0, *) {
            navigationItem.largeTitleDisplayMode = .never
        }
        
        fetchComics()
        
        // Register cells
        tableView.register(MarvelComicCell.self, forCellReuseIdentifier: "Cell")
        
        tableView.separatorStyle = .none
    }
    
    // MARK: Methods
    private func fetchComics() {
        let ts = Int(NSDate().timeIntervalSince1970.rounded())
        let hash = MD5("\(ts)\(ApiKeys.Marvel.private_key)\(ApiKeys.Marvel.public_key)").lowercased()
        let parameters: [String : Any] = [
            "ts": ts,
            "apikey": "3fd86871e60a34060e3835b703c66e55",
            "hash": hash
        ]
        
        let headers: [String : Any] = [
            "Content-Type": "application/json"
        ]
        
        let url = "https://gateway.marvel.com:443/v1/public/comics"
        Alamofire.request(url, method: .get, parameters: parameters, encoding: URLEncoding(destination: .queryString), headers: headers as? HTTPHeaders).responseJSON { (response) in
            switch response.result {
            case .failure(let error):
                print("Error: \(error.localizedDescription)")
            case .success(let data):
                // First make sure you got back a dictionary if that's what you expect
                guard let json = data as? [String : AnyObject] else {
                    return
                }
                
                if let data = json["data"] as? [String : Any], let results = data["results"] as? [[String : Any]] {
                    self.comics = results.compactMap({ MarvelComic(dict: $0) })
                    
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                    }
                }
            }
        }
    }

    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return comics.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! MarvelComicCell

        // Configure the cell...
        let comic = comics[indexPath.row]
        
        if let thumbnail = comic.thumbnail {
            cell.thumbImageView.loadImageUsingCacheWith(urlString: thumbnail)
        }
        
        cell.titleLabel.text = comic.title

        return cell
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
