//
//  HomeSectionTitleView.swift
//  Teste Qranio
//
//  Created by Felipe Israel on 19/01/20.
//  Copyright © 2020 Click. All rights reserved.
//

import UIKit

class HomeSectionTitleView: UICollectionReusableView {
    
    private lazy var leftLine: UIView = {
        let view = UIView()
        if #available(iOS 13.0, *) {
            view.backgroundColor = .secondaryLabel
        } else {
            view.backgroundColor = .darkGray
        }
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 21, weight: .heavy)
        if #available(iOS 13.0, *) {
            label.textColor = .secondaryLabel
        } else {
            label.textColor = .darkGray
        }
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private lazy var rightLine: UIView = {
        let view = UIView()
        if #available(iOS 13.0, *) {
            view.backgroundColor = .secondaryLabel
        } else {
            view.backgroundColor = .darkGray
        }
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupViews() {
        
        addSubview(leftLine)
        leftLine.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        leftLine.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        leftLine.widthAnchor.constraint(equalToConstant: 35).isActive = true
        leftLine.heightAnchor.constraint(equalToConstant: 0.5).isActive = true
        
        addSubview(titleLabel)
        titleLabel.leadingAnchor.constraint(equalTo: leftLine.trailingAnchor, constant: 8).isActive = true
        titleLabel.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        
        addSubview(rightLine)
        rightLine.leadingAnchor.constraint(equalTo: titleLabel.trailingAnchor, constant: 8).isActive = true
        rightLine.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -5).isActive = true
        rightLine.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        rightLine.heightAnchor.constraint(equalToConstant: 0.5).isActive = true
        
    }
    
}
