//
//  HomeCollectionViewController.swift
//  Teste Qranio
//
//  Created by Felipe Israel on 19/01/20.
//  Copyright © 2020 Click. All rights reserved.
//

import UIKit

class HomeCollectionViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    private lazy var btnProfileImageView: UIButton = {
        let imageView = UIButton(type: .custom)
        imageView.layer.cornerRadius = 16
        imageView.clipsToBounds = true
        imageView.contentMode = .scaleAspectFill
        imageView.setImage(UIImage(named: "userProfileImage"), for: .normal)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    private lazy var btnProfile: UIBarButtonItem = {
        let item = UIBarButtonItem(customView: btnProfileImageView)
        return item
    }()
    
    var sections: [HomeSection]!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnProfileImageView.widthAnchor.constraint(equalToConstant: 32).isActive = true
        btnProfileImageView.heightAnchor.constraint(equalToConstant: 32).isActive = true
        
        navigationItem.setRightBarButton(btnProfile, animated: true)
        
        sections = [
            HomeSection(items: [
                HomeSection.Item(gradient: Gradients.kindaBlue, icon: UIImage(named: "home.system.all_users")!, title: "Usuários Cadastrados", action: {
                    
                })
            ]),
            HomeSection(title: "Nasa", items: [
                HomeSection.Item(gradient: Gradients.pink, icon: UIImage(), title: "Foto do Dia", action: {
                    
                })
            ]),
            HomeSection(title: "Marvel", items: [
                HomeSection.Item(gradient: Gradients.yellow, icon: UIImage(named: "home.marvel.characters")!, title: "Personagens", action: {
                    self.showCharacters()
                }),
                HomeSection.Item(gradient: Gradients.orange, icon: UIImage(named: "home.marvel.comics")!, title: "Quadrinhos", action: {
                    self.showComics()
                }),
                HomeSection.Item(gradient: Gradients.greenBlue, icon: UIImage(named: "home.marvel.creators")!, title: "Criadores", action: {
                    
                }),
                HomeSection.Item(gradient: Gradients.skyBlue, icon: UIImage(named: "home.marvel.discover")!, title: "Descobrir", action: {
                    
                })
            ])
        ]

        // Register cell classes
        collectionView.register(HomeSectionTitleView.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "TitleHeaderView")
        collectionView.register(HomeSectionItemCell.self, forCellWithReuseIdentifier: "Cell")

        collectionView.alwaysBounceVertical = true
        
        collectionView.contentInset.bottom = 20
        
        DispatchQueue.main.async {
            self.startSignIn()
        }
    }
    
    // MARK: Methods
    private func startSignIn() {
        let signInStoryboard = UIStoryboard(name: "SignIn", bundle: nil)
        let viewController = signInStoryboard.instantiateInitialViewController() as! UINavigationController
        viewController.modalPresentationStyle = .fullScreen
        present(viewController, animated: true, completion: nil)
    }
    
    private func showCharacters() {
        let viewController = CharactersMarvelViewController()
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    private func showComics() {
        let viewController = ComicsMarvelViewController()
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    // MARK: UICollectionViewDataSource
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return sections.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if kind == UICollectionView.elementKindSectionHeader {
            let section = sections[indexPath.section]
            if let title = section.title, !title.isEmpty {
                let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "TitleHeaderView", for: indexPath) as! HomeSectionTitleView
                
//                headerView.backgroundColor = .groupTableViewBackground
                headerView.titleLabel.text = title
                
                return headerView
            }
            return UICollectionReusableView()
        }
        return UICollectionReusableView()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        let section = sections[section]
        if let title = section.title, !title.isEmpty {
            return CGSize(width: collectionView.bounds.width, height: 25)
        }
        return .zero
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return sections[section].items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let columns: CGFloat = 2
        let containerWidth = collectionView.bounds.width - 32 // left and right margins
        let cardWidth = (containerWidth - ((columns - 1) * 16)) / columns
        return CGSize(width: cardWidth, height: cardWidth * 0.65)
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! HomeSectionItemCell
        
        let item = sections[indexPath.section].items[indexPath.item]
        cell.gradient = item.gradient
        
        cell.iconImageView.image = item.icon
        cell.iconImageView.image = cell.iconImageView.image?.withRenderingMode(.alwaysTemplate)
        
        cell.titleLabel.text = item.title
    
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let item = sections[indexPath.section].items[indexPath.item]
        item.action()
    }

    // MARK: UICollectionViewDelegate

    /*
    // Uncomment this method to specify if the specified item should be highlighted during tracking
    override func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment this method to specify if the specified item should be selected
    override func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
    override func collectionView(_ collectionView: UICollectionView, shouldShowMenuForItemAt indexPath: IndexPath) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, canPerformAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
    
    }
    */

}
